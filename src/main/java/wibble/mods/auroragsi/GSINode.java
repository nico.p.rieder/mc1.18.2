package wibble.mods.auroragsi;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer; //import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.screens.controls.KeyBindsScreen; //import net.minecraft.client.gui.screen.ControlsScreen;
import net.minecraft.client.gui.screens.ChatScreen; //import net.minecraft.client.gui.screen.ChatScreen;
import net.minecraft.client.KeyMapping; //import net.minecraft.client.settings.KeyBinding;
import net.minecraft.client.multiplayer.ClientLevel; //import net.minecraft.client.world.ClientWorld;
import net.minecraft.world.effect.MobEffect; // Old import net.minecraft.potion.Effect;
import net.minecraft.world.effect.MobEffects; //Old import net.minecraft.potion.Effects;
import net.minecraft.world.level.dimension.DimensionType;

/** Container for the Provider data and Player data. */
public class GSINode {
    private ProviderNode provider = new ProviderNode();
    private GameNode game = new GameNode();
    private WorldNode world = new WorldNode();
    private PlayerNode player = new PlayerNode();

    public GSINode update() {
        game.update();
        world.update();
        player.update();
        return this;
    }

    /**
     * Contains data required for Aurora to be able to parse the JSON data.
     */
    private static class ProviderNode {
        private String name = "minecraft";
        private int appid = -1;
    }

    /**
     * Contains the data extracted from the game about the player.
     */
    private static class PlayerNode {
        private boolean inGame;
        private float health;
        private float maxHealth;
        private float absorption;
        private boolean isDead;
        private int armor;
        private int experienceLevel;
        private float experience;
        private int foodLevel;
        private float saturationLevel;
        private boolean isSneaking;
        private boolean isRidingHorse;
        private boolean isBurning;
        private boolean isInWater;
        private HashMap<String, Boolean> playerEffects = new HashMap<>();

        // Potion effects that will be added to the playerEffects map.
        private static final HashMap<String, MobEffect> TARGET_POTIONS;
        static {
            TARGET_POTIONS = new HashMap<>();
            TARGET_POTIONS.put("moveSpeed", MobEffects.MOVEMENT_SPEED);
            TARGET_POTIONS.put("moveSlowdown", MobEffects.MOVEMENT_SLOWDOWN);
            TARGET_POTIONS.put("haste", MobEffects.DIG_SPEED);
            TARGET_POTIONS.put("miningFatigue", MobEffects.DIG_SLOWDOWN);
            TARGET_POTIONS.put("strength", MobEffects.DAMAGE_BOOST);
            //TARGET_POTIONS.put("instantHealth", INSTANT_HEALTH);
            //TARGET_POTIONS.put("instantDamage", INSTANT_DAMAGE);
            TARGET_POTIONS.put("jumpBoost", MobEffects.JUMP);
            TARGET_POTIONS.put("confusion", MobEffects.CONFUSION);
            TARGET_POTIONS.put("regeneration", MobEffects.REGENERATION);
            TARGET_POTIONS.put("resistance", MobEffects.DAMAGE_RESISTANCE);
            TARGET_POTIONS.put("fireResistance", MobEffects.FIRE_RESISTANCE);
            TARGET_POTIONS.put("waterBreathing", MobEffects.WATER_BREATHING);
            TARGET_POTIONS.put("invisibility", MobEffects.INVISIBILITY);
            TARGET_POTIONS.put("blindness", MobEffects.BLINDNESS);
            TARGET_POTIONS.put("nightVision", MobEffects.NIGHT_VISION);
            TARGET_POTIONS.put("hunger", MobEffects.HUNGER);
            TARGET_POTIONS.put("weakness", MobEffects.WEAKNESS);
            TARGET_POTIONS.put("poison", MobEffects.POISON);
            TARGET_POTIONS.put("wither", MobEffects.WITHER);
            //TARGET_POTIONS.put("healthBoost", Effects.HEALTH_BOOST);
            TARGET_POTIONS.put("absorption", MobEffects.ABSORPTION);
            //TARGET_POTIONS.put("saturation", Effects.SATURATION);
            TARGET_POTIONS.put("glowing", MobEffects.GLOWING);
            TARGET_POTIONS.put("levitation", MobEffects.LEVITATION);
            TARGET_POTIONS.put("luck", MobEffects.LUCK);
            TARGET_POTIONS.put("badLuck", MobEffects.UNLUCK);
            TARGET_POTIONS.put("slowFalling", MobEffects.SLOW_FALLING);
            TARGET_POTIONS.put("conduitPower", MobEffects.CONDUIT_POWER);
            TARGET_POTIONS.put("dolphinsGrace", MobEffects.DOLPHINS_GRACE);
            TARGET_POTIONS.put("badOmen", MobEffects.BAD_OMEN);
            TARGET_POTIONS.put("villageHero", MobEffects.HERO_OF_THE_VILLAGE);
        }

		@SuppressWarnings("resource")
        private void update() {
            try {
                playerEffects.clear(); // clear before attempting to get the player else there may be values on the mainmenu

                // Attempt to get a player, and store their health and stuff
                LocalPlayer player = Minecraft.getInstance().player;
                assert player != null;
                health = player.getHealth();
                maxHealth = player.getMaxHealth();
                absorption = player.getAbsorptionAmount();
                isDead = !player.isAlive();
                armor = player.getArmorValue();
                experienceLevel = player.experienceLevel;
                experience = player.experienceProgress;
                foodLevel = player.getFoodData().getFoodLevel();
                saturationLevel = player.getFoodData().getSaturationLevel();
                isSneaking = player.isSteppingCarefully();
                isRidingHorse = player.isRidingJumpable();
                isBurning = player.isOnFire();
                isInWater = player.isInWater();

                // TODO: add boolean for chat open `Minecraft.getMinecraft().ingameGUI.getChatGUI().getChatOpen();`

                // Populate the player's effect map
                for (Map.Entry<String, MobEffect> potion : TARGET_POTIONS.entrySet())
                    playerEffects.put(potion.getKey(), player.getEffect(potion.getValue()) != null);

                inGame = true;

            } catch (Exception ex) {
                // If this failed (I.E. could not get a player, the user is probably not in a game)
                inGame = false;
            }
        }
    }

    /**
     * Contains the data extracted from the game about the current world.
     */
    private static class WorldNode {
        private long worldTime;
        private boolean isDayTime;
        private boolean isRaining;
        private float rainStrength;
        private int dimensionID;

		@SuppressWarnings("resource")
        private void update() {
            try {
				ClientLevel world = Minecraft.getInstance().level;
                if (world == null) {
                    worldTime = 0;
                    isDayTime = true;
                    rainStrength = 0;
                    isRaining = false;
                    dimensionID = 0;

                    return;
                }
                worldTime = world.getDayTime();
                isDayTime = world.isDay();
                rainStrength =  world.rainLevel;
                isRaining = world.isRaining();
                DimensionType dimension = world.dimensionType();
                if (DimensionType.NETHER_LOCATION.location().equals(dimension.effectsLocation()))
                    dimensionID = 1;
                else if (DimensionType.OVERWORLD_LOCATION.location().equals(dimension.effectsLocation()))
                    dimensionID = 2;
                else if (DimensionType.END_LOCATION.location().equals(dimension.effectsLocation()))
                    dimensionID = -1;
                else
                    dimensionID = 0;
            } catch (Exception ignore) {
            }
        }
    }

    /**
     * Contains lists of any used keys and any that are conflicting.
     */
    private static class GameNode {
        private KeyMapping[] keys;
        private boolean controlsGuiOpen;
        private boolean chatGuiOpen;

        private void update() {
            Minecraft mc = Minecraft.getInstance();
            controlsGuiOpen = mc.screen instanceof KeyBindsScreen;
            chatGuiOpen = mc.screen instanceof ChatScreen;
            keys = null;
            if(controlsGuiOpen)
                keys = mc.options.keyMappings;
        }
    }
}
